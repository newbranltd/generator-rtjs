/**
 * all the private methods move here
 */
const fs = require('fs');
const path = require('path');
const join = path.join;
const chalk = require('chalk');
const exec = require('child_process').exec;
const when = require('when');
const yosay = require('yosay');
const BaseClass = require('./base-class.js');

const version = require(
    join(__dirname, '..', 'package.json')
).version;

module.exports = class PrivateClass extends BaseClass {
  // Yeoman style privates
  __getNpmLibDeps() {
      // @20170901 falling back to use bower, still no good solution at the moment
      return [];
  }
  /**
   * Get the list of bower dependencies
   * @return {array} list
   */
   __getBowerLibDeps() {
      let list = this.npmList.based; // @20170901 riot and riot-route are base deps
      if (this.props.cssFramework === 'none') {
          return list;
      }
      return list.concat(
          this.cssFrameworks.filter(e => {
              return e.value === this.props.cssFramework;
          }).map(e => {
              return e.npm;
          })
      );
  }
  /**
   * Getting the devDepenecies
   * @return {array} list
   */
   __getDevLibDeps() {
      // Append the test required deps here
      let list = this.npmList.dev.concat(this.npmList.jest);
      // Start adding other deps
      if (this.props.useGit) {
          list = list.concat(this.npmList.git);
      }
      if (this.props.cssDevStyle === 'scss') {
          return list.concat(this.npmList.scss);
      }
      return list.concat(this.npmList.postcss);
  }
  /**
   * Display a message during installation
   * @param {array} list
   * @param {string} title
   */
  __installMsg(list, title = 'INSTALLING') {
      this.log(chalk.white('INSTALLING ' + title));
      list.forEach(l => {
          this.log(chalk.white(' -> ' + l));
      });
  }
  /**
   * Core bower dependencies
   * @return {array}
   */
  __jsBowerLibDeps() {
      const list = this.__getBowerLibDeps().concat(
          this.props.optionalDeps
      );
      this.__installMsg(list, 'DEPENDECIES');
      return list;
  }
  /**
   * Dev dependencies
   * @return {array}
   */
  __jsDevLibDeps() {
      const list = this.__getDevLibDeps();
      this.__installMsg(list, 'DEV DEPENDECIES');
      return list;
  }
  /**
   * 2017-06-06 going back to use bower because using npm to manage browser deps is a joke
   * @param {function} callback
   */
  __cb(callback) {
      return (typeof callback === 'function') ? callback : function () {};
  }

  /**
   * Have Yeoman greet the user.
   */
  __begin() {
      this.log(
          yosay(
              this.langObj.greeting.replace(
                  '{{generatorName}}',
                  chalk.red('generator-rtjs')
              ).replace(
                  '{{version}}',
                  version
              )
          )
      );
  }
  /**
   * Final message stuff
   */
  __final() {
      // "gulpjs/gulp.git#4.0", --> gulp 4 keep on failing --> 2017-06-03 turns out it was npm@5.0.1 bug
      this.config.save(); // Save the preference
      this.log(
          this.langObj.bye
      );
      // Should execute the `gulp dev` now
      this.__gulpDev();
  }
  /**
   * Return the list of prompts
   * @param {boolean} yanInstalled from _checkIfYarnInstalled
   */
  __getPrompts(yarnInstalled) {
      let installChoices = [
          {name: 'npm', value: 'npmInstall'},
          {name: 'yarn', value: 'yarnInstall'}
      ];
      const cn = this.lang === 'cn';
      if (cn) {
          installChoices.push({name: 'cnpm', vlaue: 'cnpm'});
      }
      return [{
          type: 'input',
          name: 'appName',
          message: this.langObj.appName,
          default: this.suggestAppName
      },
      {
          type: 'list',
          name: 'cssFramework',
          choices: this.cssFrameworks,
          message: this.langObj.cssFramework
      }, {
          when: props => {
              if (this.requireScss.indexOf(props.cssFramework) > -1) {
                  props.cssDevStyle = 'scss';
                  return false;
              }
              return props.cssFramework !== 'none';
          },
          type: 'list',
          name: 'cssDevStyle',
          message: this.langObj.cssDevStyle,
          choices: this.cssDevStyles
      }, {
          when: props => {
              // console.log(props.cssFramework , this.optionals);
              if (this.hasJquery.indexOf(props.cssFramework) > -1) {
                  this.optionals.shift();
              }
              return true;
          },
          type: 'checkbox',
          name: 'optionalDeps',
          choices: this.optionals, // need to run a filter here when selected certain css framework
          message: this.langObj.optionals
      }, {
          type: 'confirm',
          name: 'useGit',
          message: this.langObj.useGit,
          default: true
      }, {
          when: props => {
              if (cn) { // Then we need to let them choose
                  return cn;
              }
              if (yarnInstalled) {
                  props.installer = 'yarnInstall';
              }
              return !yarnInstalled;
          },
          type: 'list',
          name: 'installer',
          message: this.langObj.installer,
          choices: installChoices,
          default: cn ? 'cnpm' : 'npmInstall'
      }];
  }
}
