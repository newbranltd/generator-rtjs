/**
 * installer methods move here
 */
const fs = require('fs');
const path = require('path');
const join = path.join;
const chalk = require('chalk');
const childProcess = require('child_process');
const exec = childProcess.exec;
const spawn = childProcess.spawn;
const when = require('when');
const glob = require('glob');
const config = require(
    join(__dirname, 'config.json')
);
const appPath = config.appPath;
const PrivateClass = require('./private.js');

module.exports = class InstallerClass extends PrivateClass {
  /**
   * run constructor as well as setup some test option
  constructor(args, opts) {
    super(args, opts);
    if (process.env.NODE_ENV==='test') {
      if (this.props && !this.props.installer) {
        this.props.installer = 'npmInstall';
      }
    }
  }
  */
  /**
   * Run the gulp command at the end
   * @2017-07-09 add the git sub command and switch if useGit
   */
  __gulpDev() {
      const cmd = this.props.useGit ? 'project:init' : 'dev';
      const ls = spawn('gulp', [cmd]);
      this.log(
          chalk.white(
              this.langObj.runningGulpForYou.replace('{{cmd}}', cmd)
          )
      );
      ls.stdout.on('data', data => {
          this.log(`${data}`);
      });
      ls.stderr.on('data', data => {
          this.log(chalk.yellow(`stderr: ${data}`));
      });
      ls.on('close', code => {
          this.log(chalk.red(`child process exited with code ${code}`));
      });
  }
  /**
   * This one needs a lot of calculations ...
   */
  __copyAppFiles() {
      const basePath = join(appPath, 'scripts', 'components');
      const framework = this.props.cssFramework;
      const frameworkBase = join(basePath, framework);
      // Babel profile - should be in the root folder
      // @UPDATE we might need to have separate babel file for test and app code
      this._copy(join(appPath, 'babelrc'), join('.babelrc'));
      // Main.js
      this._copyTpl(
          [appPath, 'scripts', 'main.js'],
          null, // [appPath, 'scripts', 'main.js'],
          {cssFramework: framework}
      );
      // Tag files
      this._copy(
           join(basePath, 'app', 'app.tag')
      );
      this._copy(
           join(frameworkBase, 'home', 'home.tag'),
           join(basePath, 'home', 'home.tag')
      );
      this._copy(
           join(frameworkBase, 'about', 'about.tag'),
           join(basePath, 'about', 'about.tag')
      );
      // The rest of the apps
      this._copy(
           join(frameworkBase, 'app', 'app-route.tag'),
           join(basePath, 'app', 'app-route.tag')
      );
      this._copyTpl(
          [frameworkBase, 'app', 'app-nav.tag'],
          [basePath, 'app', 'app-nav.tag'],
          {appName: this.props.appName}
      );
  }
  /**
   * Copy the test files
   */
  __copyTestFiles() {
      const testPath = config.testPath;
      this._copy(
          join(testPath, 'components', 'about', 'about.test.js')
      );
      this._copy(
          join(testPath, 'components', 'home', 'home.test.js')
      );
  }
  /**
   * Default type to css - @TODO remove this reference we are going back to use bower
   * @param {string} type
   */
  __getCssFilePath(type = 'css') {
      const paths = {
          bootstrap4: {
              css: join('bootstrap', 'dist', 'css', 'bootstrap.css'),
              scss: join('bootstrap', 'scss', 'bootstrap.scss')
          },
          materialui: {
              css: join('daemonite-material', 'css', 'material.css'),
              scss: join('daemonite-material', 'assets', 'sass', 'material.scss')
          },
          foundation6: {
              css: join('foundation-sites', 'dist', 'css', 'foundation.css'),
              scss: join('foundation-sites', 'assets', 'foundation.scss')
          },
          tachyons: {
              css: join('tachyons-css', 'css', 'tachyons.css'),
              scss: join('tachyons-css', 'css', 'tachyons.css')
          },
          bmd4: {
              css: join('bootstrap-material-design', 'scss', '_core.scss'), // fucking annoying
              scss: join('bootstrap-material-design', 'scss', '_core.scss')
          },
          none: {
              css: null,
              scss: null
          }
      };
      return paths[this.props.cssFramework][type];
  }
  /**
   * For the index file
   */
  __copyHtmlFile() {
      let opt = {
          title: this.props.appName,
          lang: this.lang,
          cssFilePath: false,
          cssFramework: this.props.cssFramework
      };
      if (this.props.cssFramework !== 'none' && this.props.cssDevStyle === 'css') {
          opt.cssFilePath = this.__getCssFilePath();
      }
      this._copyTpl(
           [appPath, 'index.html'],
           null, // [appPath, 'index.html'],
           opt
      );
  }
  /**
   * What the name said
   */
  __copyStyleFile() {
      if (this.props.cssDevStyle === 'scss') {
          let basePath = this.__getCssFilePath('scss');
          this._copyTpl(
               [appPath, 'styles', 'main.scss'],
               null, // [appPath, 'styles', 'main.scss'],
               {
                   cssFrameworkFilePath: basePath,
                   appName: this.props.appName
               }
          );
      } else {
          this._copyTpl(
              [appPath, 'styles', 'main.css'],
              null,
              {appName: this.props.appName}
          );
      }
  }
  /**
   * What the name said
   */
  __copyBaseFiles() {
      this._copy('gitignore', '.gitignore');
      ['eslintrc.js'].forEach(file => {
          this._copy(file);
      });
      // 'nb.config.js'
      this._copyTpl(
          'nb.config.js',
          null,
          config
      );
  }
  /**
   * Copy every gulp related stuff here
   */
  __copyGulpFiles() {
      const isCss = this.props.cssDevStyle === 'css';
      const isSass = this.props.cssDevStyle === 'scss';
      this._copyTpl(
          'gulpfile.js',
          null, // 'gulpfile.js',
          {
              css: isCss,
              sass: isSass,
              useGit: this.props.useGit
          }
      );
      const templatePath = this.sourceRoot();
      glob(join(templatePath, 'gulp-scripts', '**', '*.*'), (er, files) => {
          if (er || !files.length) {
              this.log(chalk.red('globbing gulp files error!'));
              throw er;
          }
          files.forEach(src => {
              // Skip the file based on how they develop their style
              const filename = path.basename(src);
              if (isCss && filename === 'sass.js') {
                  return;
              }
              if (isSass && filename === 'css.js') {
                  return;
              }
              // Not a default template path
              this._copy(
                  src.replace(templatePath, ''),
                  join('gulp-scripts', path.basename(src))
              );
          });
      });
  }
  /**
   * Just copy the assets folder there is nothing but just a README file
   */
  __copyAssets() {
      this._copy(
          join('app', 'assets', 'README.md')
      );
  }
  /**
   * this need to be the last call because yarn bug that wipe out the bower folder!
   * @param {function} cb
   */
  __installBowerDeps(cb) {
      // Need to combine the option and do the installation here
      return this.bowerInstall(
          this.__jsBowerLibDeps(),
          {save: true},
          this.__cb(cb)
      );
  }
  /**
   * Install the npm deps
   * @param {function} cb
   */
  __installNpmDeps(cb) {
      const packages = this.__getNpmLibDeps();
      const opt = {save: true};
      if (this.props.installer === 'cnpm') {
          return this.runInstall('cnpm', packages, opt, this.__cb(cb));
      }
      // Const opt = (this.props.installer === 'yarnInstall') ? {dev: true} : {save: true};
      return this[this.props.installer](
            packages,
            opt,
            this.__cb(cb)
      );
  }
  /**
   * Yarn or npm here (@TODO auto detect what they have got)
   * @param {function} cb
   */
  __installDevDeps(cb) {
      const packages = this.__jsDevLibDeps();
      if (this.props.installer === 'cnpm') {
          return this.runInstall('cnpm', packages, {saveDev: true}, this.__cb(cb));
      }

      const opt = (this.props.installer === 'yarnInstall') ? {dev: true} : {saveDev: true};
      return this[this.props.installer](
          packages,
          opt,
          this.__cb(cb)
      );
  }
  /**
   * generate README.md
   */
  __createReadMe(dirname) {
    this._copyTpl(
        'README.md',
        'README.md',
        {
          appName: this.props.appName,
          dirname
        }
    );
  }
}
