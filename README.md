# generator-rtjs for Riot 3 [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]

> Yeoman Generator for Riot 3 with gulp, rollup and other good stuff

## Installation

First, install [Yeoman](http://yeoman.io) and `generator-rtjs` using [npm](https://www.npmjs.com/) (we assume you have already installed [node.js](https://nodejs.org/)).

```bash
  $ npm install --global yo
  $ npm install --global generator-rtjs
```

or shorthand:

```bash
  $ npm i yo generator-rtjs -g
```

Then generate your new project:

```bash
mkdir your-project-name && cd your-project name && yo rtjs your-project-name-if-different-from-folder-name
```

Again we have English and Chinese support

```bash
yo rtjs your-new-project-name --lang=cn
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

# IMPORTANT STUFF TO KNOW BEFORE YOU USE THIS GENERATOR

Here are a few things you need to know (do) before you can use this generator for your next Riot (v.3) projects:

## Gulp (V4)

This is not official release yet. So you need to change a few of your own settings before you can use it.

If you have existing gulp (pre 4) installed globally. Then you need to uninstall it first:

    $ npm remove gulp -g

Don't worry, you are going to install the new `gulp-cli`. It works with older projects:

    $ npm install gulp-cli -g

Please check out [this article from liquidlight.co.uk](https://www.liquidlight.co.uk/blog/article/how-do-i-update-to-gulp-4/) for more detail.

## CSS Frameworks

We offer you 3+1 choices at the moment:

1. [Bootstrap 4 (alpha 6)](https://v4-alpha.getbootstrap.com/)
2. [Daemonite Material for Bootstrap 4 (alpha 6)](https://daemonite.github.io/material/)
3. [Foundation 6](http://foundation.zurb.com/learn/tutorials.html)

As you can see there are 2 both in alpha stage, use it with caution. We also
offer you a new option to **NOT** include a CSS framework, you can DIY later on.

## Rollup

I never hide my dislike toward webpack and browsersify :p and I am using none of them.
[Rollup.js](https://rollupjs.org/) is at the heart of the build process.
Go learn about it, and start loving it. UI **DO NOT** need to be complicated!

## Bower

We are still using bower, yes. Why?

Important things should say 3 times:

**Using npm to manage browser dependencies is a joke!**

### Using npm to manage browser dependencies is a joke!

# Using npm to manage browser dependencies is a joke!

This is a tool for people whom are doing this line of work for living (paying rent anyone?)
Not for those fools who ask questions like "X vs Y ...", "What is best ..."

### Article: The reason why I think those cool kids can go fcuk themselves and stop smoking banana skins

npm born out of the nodejs world and, its for *SERVER SIDE* Javascript (Yes, nodejs is NOT *FRONT END* It doesn't run in browser!!!)
and, some where down the line some fcuking idiots come in the scene and, start doing front end job - without understanding even how
browser javascript works (there are different Javascript(s) in this world, do you know that?) and they start making this shit up
about merging the front end and the backend - Netscape tried that over 2 decades ago, and they disappeared.

I am not saying that's impossible, but programming world is huge, as huge as a galaxy, and different disciplines required different
thinking. The more you try to use *ONE WAY* to deal with everything, you only ended up with dealing with NOTHING.

Let's take a look at an example when you use bower how do you deal with the vendor library injection (during DEV)
if anyone has use my other generator's before, you should have seen the following.

1. I need a new library. Run `bower install xyz --save`
2. My gulp script pick up the bower files has change, kicks in and scan the bower.json, re-run the gulp script.
3. done! Just call your xyz in your other script that needs it.

How do you do that using npm? Let say you are using webpack.

1. First install it `npm install xyz --save`
2. In your js files that requires it. `import xyz from 'xyz'`;
3. Your bundling should rerun and re-create the entire bundle - with your library files.

OK they look similar, and easy enough, right?

How about distribution. Create a vendor separate files, oh how about your deploy environment is hip and edgy using HTTP2?
With my other setup, it's simple as - do nothing and it will work, or use the bower.json file to grab the vendor and bundle it together and you get your
vendor.js

You are fcuked if you are using npm and webpack - do that in the 90's style and read every package.json file and find out the path and write them manually
in your index.html file. Or take a look at this [question from stackoverflow](https://stackoverflow.com/questions/30329337/how-to-bundle-vendor-scripts-separately-and-require-them-as-needed-with-webpack).
It makes me laugh.

I rest my case.

_Unfortunately, I need to drop bower in the next release, because of a lot of packages dropping their support.
But package writer are shit at supporting standard, such as the "browser" field_

## Test Framework

We are moving testing framework to [Jest](https://facebook.github.io/jest/) now. And since it's not really official supported by [Jest](https://facebook.github.io/jest/).
There might be some thing not working, we are discovering it's capability (In fact this generator is tested by Jest also)
So learn along with us? Leave your comments, tips or any problems you encounter etc here in github. Appreciated.

## Sub generators

There are three at the moment:

1. mixin
2. tag
3. service

```
yo rtjs:mixin your-mixin-name
```

or

```
yo rtjs:tag your-tag-name
```

You can pass the `c` (or `component`) option

```
yo rtjs:tag your-tag-name --c=home
```

That will create your component inside your existing home tag folder. Type `yo rtjs:tag --help` for USAGE (same for `mixin`)

Service sub generator has some different setup. It will check if you have certain package install (from bower).
And ask you if you want to import them into your service class. @TODO it will also check your existing services folder
and generate list for you to extend your new service from.

**For more detail, check out the [wiki page](https://github.com/joelchu/generator-rtjs/wiki) here** (again, it was for the last version. We will update it as soon as possible)

## License

MIT © [joelchu](https://to1source.com)


[npm-image]: https://badge.fury.io/js/generator-rtjs.svg
[npm-url]: https://npmjs.org/package/generator-rtjs
[travis-image]: https://travis-ci.org/joelchu/generator-rtjs.svg?branch=master
[travis-url]: https://travis-ci.org/joelchu/generator-rtjs
[daviddm-image]: https://david-dm.org/joelchu/generator-rtjs.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/joelchu/generator-rtjs
