'use strict';

const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

const config = require('../lib/config.json');
const join = path.join;
const mixinName = 'my-new-mixin';
const ext = 'js';

describe('generator-rtjs:mixin', () => {
  test('creates mixin files', () => {
    return helpers.run(
            join(__dirname, '../generators/mixin')
        ).withArguments([
          mixinName
        ]).then(() => {
          assert.file([
            join(config.appPath, 'scripts', 'mixins', [mixinName, ext].join('.')),
            join(config.testPath, 'mixins', [mixinName, 'test', ext].join('.'))
          ]);
        });
  });
  // @TODO test with module
  // test('create mixin files within module' , () => {});
});
