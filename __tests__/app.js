'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

describe('generator-rtjs:app', () => {
  it('creates package.json and bower.json', () => {
    return helpers.run(
            path.join(__dirname, '../generators/app')
        ).withOptions({
          lang: 'en',
          'skip-installation-for-test-purpose-only': true
        }).withArguments([
          'my-test-app'
        ]).withPrompts({
          useGit: true
        }).withPrompts({
          cssFramework: 'none'
        }).withPrompts({
          installer: 'npm'
        }).then(() => {
          // @TODO add the full list of expected create files here
          assert.file([
            'package.json',
            'bower.json',
            'README.md'
          ]);
        });
  });
});
