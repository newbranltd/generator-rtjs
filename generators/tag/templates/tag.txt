<<%= tagName %>>

	<div>
		{opts.name}
		<!-- your tag -->
	</div>

	<% if (isStateless) { %>
	<style>
		/* your styling */
	</style>
	<% } %>

    <script>
		/* your JS code */
	</script>

</<%= tagName %>>
