# <%=  appName %>

To start develop:

```sh
  $ cd <%= dirname %>
  $ gulp dev
```

To build project:

```sh
  $ gulp build
```

For more options please visit [generator-rtjs home page](https://github.com/joelchu/generator-rtjs)
