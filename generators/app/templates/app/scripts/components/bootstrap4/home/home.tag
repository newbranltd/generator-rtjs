<home>
    <div class="card" id="home-card">
        <div class="card-block">
            <a href="https://v4-alpha.getbootstrap.com">
                <h1>Bootstrap 4 starter template</h1>
            </a>
            <p class="lead">
                Use this document as a way to quickly start any new project.
                <br />
                All you get is this text and a mostly barebones HTML document.
            </p>
        </div>
    </div>

    <style>
        #home-card {
            margin-top: 65px;
        }
    </style>

</home>
