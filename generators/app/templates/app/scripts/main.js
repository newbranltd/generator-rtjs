'use strict';
/**
 * The entry point of the whole app
 */
import riot from 'riot';

// mounting the main tag
import 'components/app/app.tag';
riot.mount('app');
