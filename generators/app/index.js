/**
 * Rtjs:app
 * Riot.js 3 generator
 */
/*
          \
         /  `.-~~--..--~~~-.
        <__                 `.
     ~~~~~~~~~~~~~~~~.~~~~,','
        TOMATO        .~~_,'
*/
const chalk = require('chalk');
// Additional deps
const path = require('path');
const glob = require('glob');
const username = require('git-user-name');
const spawn = require('child_process').spawn;
// Const extend = require('util')._extend;
// Base class extend from yeoman-generator
const join = path.join;
const dirname = join(__dirname, '..', '..');
const version = require(
    join(dirname, 'package.json')
).version;
const BaseClass = require(
    join(dirname, 'lib', 'installer.js')
);
const config = require(
    join(dirname, 'lib', 'config.json')
);

const appPath = config.appPath;

// Const srcPath = config.srcPath; // use this when we have the rapture.js integration
// const destPath = config.destPath;
const developerName = 'to1source.com';
// Properties
const defaultLang = 'en';
// Class
module.exports = class extends BaseClass {
    /**
     * class constructor
     * @param {array} args
     * @param {object} opts
     */
    constructor(args, opts)
    {
        super(args, opts);
         // Determine the app name
        this.suggestAppName = (args.length) ? args[0] : path.basename(this.contextRoot);
         // Skip installation
        this.skipInstallation = opts['skip-installation-for-test-purpose-only']; // Should never use this!
         // Language options they need to provide the flag --lang=cn to switch
        this.lang = opts.lang ? opts.lang.toLowerCase() : defaultLang;
         // Try to use Monad but the problem is the value need to transform
         // and hard to switch to the raw value! Now I understand why it needs to be immutable!
        this._loadLangFile(this.lang);
         // Load the npm-list.json file
        this.npmList = require(join(__dirname, 'templates', 'npm-list.json'));
         // Store the lang and appName useful later
        this.config.set({
            appName: this.appName,
            lang: this.lang
        });
        // Console.log(this.config.set.toString()); // what the hell is inside?
        // @20170901 if we switch to npm then use the optional otherwise use bower
        this.optionals = this.npmList.bower.map(optional => {
            optional.checked = true;
            return optional;
        });
    }
    // HOOKS
    /**
     * the actual q & a
     */
    prompting() {
        this.__begin();
        return this._checkIfYarnInstalled().then(yarnInstalled => {
            return this.prompt(
                this.__getPrompts(yarnInstalled)
            ).then(props => {
                this.props = props;
            });
        });
    }
    /**
     * Creating files
     */
    writing() {
        ['package.json', 'bower.json'].forEach(file => {
            this.fs.copyTpl(
                this.templatePath(file),
                this.destinationPath(file),
                {
                    appName: this.props.appName,
                    author: username() || developerName,
                    cssFramework: this.props.cssFramework
                }
            );
        });
        // Copy tasks
        this.__copyGulpFiles();
        this.__copyBaseFiles();
        this.__copyAppFiles();
        this.__copyTestFiles();
        this.__copyHtmlFile();
        this.__copyStyleFile();
        this.__copyAssets();
        this.__createReadMe(path.basename(this.contextRoot));
    }
    /**
     * Running the dependencies installation
     */
    install() {
        if (!this.skipInstallation) { // For testing purpose only!
            this.__installNpmDeps(() => {
                this.__installDevDeps(() => {
                    this.__installBowerDeps(
                        () => this.__final()
                    );
                });
            });
        }
    }
};

// --- EOF ---
